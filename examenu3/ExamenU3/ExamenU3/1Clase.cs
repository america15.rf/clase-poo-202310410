﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU3
{
    class _1Clase
    {
        private double b;
        private double h;
        public double Resultado;

        public _1Clase(double b, double h, double Resultado)
        {
            this.b = b;
            this.h = h;
            this.Resultado = Resultado;
        }

        public _1Clase()
        {
            
        }

        public void Introducir()
        {
            Console.WriteLine("Introducir Datos para Calcular el Area de un Triangulo");
            Console.WriteLine();
            Console.WriteLine("Introducir Base:");
            b = double.Parse(Console.ReadLine());
            Console.WriteLine("Introducir Altura:");
            h = double.Parse(Console.ReadLine());

        }
        private void Procedimiento()
        {
            Resultado = (b * h)/2;
        }
        public void AccesoProcedimiento()
        {
            Procedimiento();
        }
        public void ImprimirDatos()
        {
            Console.WriteLine();
            Console.WriteLine("El Area del Triangulo es: " + Resultado);
        }

        ~_1Clase()
        {
            this.b = 0;
            this.h = 0;
            this.Resultado=0;
            Console.WriteLine("Este es el Constructor");

        }
    }
}
