﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU3
{
    class Program
    {
        static void Main(string[] args)
        {
            
            _1Clase obj=new _1Clase();
            obj.Introducir();
            obj.AccesoProcedimiento();

            Herencia1Clase obj2 = new Herencia1Clase();
            Hijo hijo = new Hijo();
            hijo.AccesoProcedimiento();

            Console.ReadKey();

        }
    }
}
