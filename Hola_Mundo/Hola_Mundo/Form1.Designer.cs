﻿namespace Hola_Mundo
{
    partial class Hello_World
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Hello_World));
            this.btnHolaMundo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnHolaMundo
            // 
            this.btnHolaMundo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHolaMundo.BackgroundImage")));
            this.btnHolaMundo.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btnHolaMundo.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHolaMundo.ForeColor = System.Drawing.Color.Black;
            this.btnHolaMundo.Location = new System.Drawing.Point(129, 67);
            this.btnHolaMundo.Name = "btnHolaMundo";
            this.btnHolaMundo.Size = new System.Drawing.Size(116, 36);
            this.btnHolaMundo.TabIndex = 0;
            this.btnHolaMundo.Text = "Hola Mundo";
            this.btnHolaMundo.UseVisualStyleBackColor = true;
            this.btnHolaMundo.Click += new System.EventHandler(this.btnHolaMundo_Click);
            // 
            // Hello_World
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(348, 173);
            this.Controls.Add(this.btnHolaMundo);
            this.Name = "Hello_World";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnHolaMundo;
    }
}

