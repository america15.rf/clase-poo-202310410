﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class MonitordeViaje
    {
        public void Dates()
        {
            int nro_monitoreo = 156;
            string diagnostic_pac = "Enfermo";
            int hora = 5;
            string pulso = "medio";
            string presion = "media";
            string lat_fetales = "ninguno";
            int num_contracciones = 2;
            int hem_cantidad = 4;
            int convulsiones_hra = 1;
            string paro_cardiaco = "no";
            string otros = "ninguno";

            Console.WriteLine("Datos");
            Console.WriteLine();

            Console.WriteLine("Nro monitoreos: " + nro_monitoreo);
            Console.WriteLine();
            Console.WriteLine("Diagnostico pac: " + diagnostic_pac);
            Console.WriteLine();
            Console.WriteLine("Hora: " + hora);
            Console.WriteLine("Pulso: " + pulso);
            Console.WriteLine("Presion: " + presion);
            Console.WriteLine("Lat fetales: " + lat_fetales);
            Console.WriteLine("Num de Conytacciones: " + num_contracciones);
            Console.WriteLine("Hem Cantidad: " + hem_cantidad);
            Console.WriteLine("Convulsiones por hora: " + convulsiones_hra);
            Console.WriteLine("Paro Cardiaco: " + paro_cardiaco);
            Console.WriteLine("Otros: " + otros);
        }
        public void Act()
        {
            Console.WriteLine("¿Cuantos pasajeros se analizaran?");
            int p = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < p; i++)
            {
                Console.WriteLine("Ingresar Nro Monitoreo");
                int Nro = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresar Diagnostico pac");
                string D = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Ingresar Hora");
                int Hra = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresar Pulso");
                string pul = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Ingresar Presion");
                string prs = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Ingresar Lat fetales");
                string lat = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Ingresar Numeros de contracciones");
                int c = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresar hem Cantidad");
                int hem = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresar Convulsiones por hora");
                int convu = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresar paro cardiaco");
                string pcard = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Ingresar Otros");
                string ot = Convert.ToString(Console.ReadLine());
            }
        }
    }
}
