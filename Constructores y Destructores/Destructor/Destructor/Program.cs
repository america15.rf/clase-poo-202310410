﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Destructor
{
    class Prueba
    {
        private int p;
        static void Main(string[] args)
        {
            Prueba p = new Prueba(5);
        }
        public Prueba(int x)
        {
            System.Console.Write("Creado objeto Prueba con x={0}", x); Console.ReadLine();
            Console.ReadLine();
        }
        
        ~Prueba()
        {
            /*Destructor*/
            this.p = 0;
        }

    }
}

