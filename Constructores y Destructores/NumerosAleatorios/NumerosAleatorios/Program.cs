﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumerosAleatorios
{
    class Aleatorios
    {
        private int r;

        public Aleatorios(int r)
        {
            /*Constructor*/
            this.r = r;
        }
        static void Main(string[] args)
        {
            
            Random r = new Random();

            Console.WriteLine("10 Numeros Aleatorios del 0 al 250");
            Console.ReadLine();
            for (int i = 0; i < 10; i++)
            {
                
                Console.WriteLine(r.Next(0, 250));

                
                Console.ReadLine();
            }
            Console.WriteLine("FINAL");

        }

        ~Aleatorios()
        {
            /*Destructor*/
            this.r = 0;
        }

    }
    
}
