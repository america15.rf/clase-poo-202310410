﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumerosPrimo
{
    class NumerosPrimos
    {
        private int num;

        public NumerosPrimos(int num)
        {
            /*Constructor*/
            this.num = num;
        }
        static void Main(string[] args)
        {
            {

                int num;
                bool rompe = false;
                string Cadnumero;
                Console.WriteLine("INGRESAR UN NUMERO");
                Cadnumero = Console.ReadLine();
                num = Convert.ToInt32(Cadnumero);
                for (int i = 2; i < num; i++)
                {
                    if (num % i == 0)
                    {
                        rompe = true;
                        break;
                    }
                }
                if (rompe)
                {
                    Console.WriteLine("{0} no es primo", num);
                }
                else
                {
                    Console.WriteLine("{0} es primo", num);
                }
            }
            Console.ReadLine();
        }

        ~NumerosPrimos()
        {
            /*Destructor*/
            this.num= 0;
        }
           
    }
}
