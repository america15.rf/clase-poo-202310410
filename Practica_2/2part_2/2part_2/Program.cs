﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2part_2
{
    class Program
    {
        static void Main(string[] args)
        {
            circunferencia o = new circunferencia();
            o.Rueda();
            o.Moneda();
            Console.ReadLine();
        }

        class circunferencia
        {
            public double radio=0, R, M;
            public const double PI = 3.1415;

            public void Rueda()
            {
                Console.WriteLine("Calcular area de la Rueda.");
                Console.ReadLine();
                Console.WriteLine("Inicialize el radio de la Rueda con el valor de 10: ");
                radio = double.Parse(Console.ReadLine());
                R = radio * radio * PI;
                Console.WriteLine("El Area de la Rueda es: " + R);
                
                Console.WriteLine("Calcular perimetro de la Rueda.");
                Console.WriteLine("El Perimetro de la Rueda es {0} ", PI * (2 * radio));
                Console.ReadLine();

            }

            public void Moneda()
            {
                Console.WriteLine("Calcular area de la Moneda.");
                Console.ReadLine();
                Console.WriteLine("Inicialize el radio de la Moneda con el valor de 1: ");
                radio = double.Parse(Console.ReadLine());
                M = radio * radio * PI;
                
                Console.WriteLine("El Area de la Moneda es: " + M);
                
                Console.WriteLine("Calcular perimetro de la Moneda.");
                
                Console.WriteLine("El Perimetro de la Moneda es {0} ", PI * (2 * radio));
            }
        }

       
    }
}
