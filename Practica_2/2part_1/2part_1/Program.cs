﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2part_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Circunferencia o = new Circunferencia();
            o.calculararea();
            o.calcularperimetro();
            Console.ReadLine();

        }

        class Circunferencia
        {
            
            public double radio=0, n=0;
            public const double PI = 3.1415;
            
            public void calculararea()
            {
                Console.WriteLine("Encontrar AREA.");
                
                Console.Write("Ingrese el radio del circulo: ");
                radio = double.Parse(Console.ReadLine());
                n = radio*radio*PI;
                Console.WriteLine("El Area del Circulo: " + n);

            }

            public void calcularperimetro()
            {
                Console.WriteLine("Encontrar perimetro.");
                Console.WriteLine("El perimetro del circulo es {0} ", 3.141592 * (2 * radio));//formula perimetro P=pi.2r

            }
        }
    }
}
