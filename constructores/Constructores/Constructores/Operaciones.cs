﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructores
{
    class Operaciones
    {
        public int multa;

        public Operaciones(int multa)
        {
            this.multa = multa;
        }

        public int CalcularMulta()
        {
            int resultado = 0;

            switch (this.multa)
            {
                case 0:
                    resultado = 180;
                    break;
                default:
                    resultado = 0;
                    break;

                case 1:
                    resultado = 250;
                    break;
                case 2:
                    resultado = 150;
                    break;
                case 3:
                    resultado = 400;
                    break;
            }

            return resultado;
        }
    }
}
