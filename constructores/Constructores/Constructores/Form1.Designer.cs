﻿namespace Constructores
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cbxSelecMults = new System.Windows.Forms.ComboBox();
            this.lblSelecMulta = new System.Windows.Forms.Label();
            this.lblPagarMulta = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.lblresultado = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.lblSigno = new System.Windows.Forms.Label();
            this.lblpesos = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbxSelecMults
            // 
            this.cbxSelecMults.BackColor = System.Drawing.Color.DimGray;
            this.cbxSelecMults.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxSelecMults.ForeColor = System.Drawing.Color.White;
            this.cbxSelecMults.FormattingEnabled = true;
            this.cbxSelecMults.Items.AddRange(new object[] {
            "Mal Estacionamiento",
            "Exceso de Velocidad",
            "Pasarse un Alto",
            "Exceso de Alcohol"});
            this.cbxSelecMults.Location = new System.Drawing.Point(307, 65);
            this.cbxSelecMults.Name = "cbxSelecMults";
            this.cbxSelecMults.Size = new System.Drawing.Size(203, 28);
            this.cbxSelecMults.TabIndex = 0;
            // 
            // lblSelecMulta
            // 
            this.lblSelecMulta.AutoSize = true;
            this.lblSelecMulta.BackColor = System.Drawing.Color.Gray;
            this.lblSelecMulta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSelecMulta.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelecMulta.ForeColor = System.Drawing.Color.White;
            this.lblSelecMulta.Location = new System.Drawing.Point(118, 71);
            this.lblSelecMulta.Name = "lblSelecMulta";
            this.lblSelecMulta.Size = new System.Drawing.Size(141, 22);
            this.lblSelecMulta.TabIndex = 1;
            this.lblSelecMulta.Text = "SELECCIONA MULTA";
            // 
            // lblPagarMulta
            // 
            this.lblPagarMulta.AutoSize = true;
            this.lblPagarMulta.BackColor = System.Drawing.Color.DimGray;
            this.lblPagarMulta.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPagarMulta.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPagarMulta.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblPagarMulta.Location = new System.Drawing.Point(118, 147);
            this.lblPagarMulta.Name = "lblPagarMulta";
            this.lblPagarMulta.Size = new System.Drawing.Size(128, 22);
            this.lblPagarMulta.TabIndex = 2;
            this.lblPagarMulta.Text = "PAGO POR MULTA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.DarkGreen;
            this.label3.Font = new System.Drawing.Font("MV Boli", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(259, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 21);
            this.label3.TabIndex = 3;
            // 
            // btnCalcular
            // 
            this.btnCalcular.BackColor = System.Drawing.Color.Maroon;
            this.btnCalcular.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcular.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCalcular.Location = new System.Drawing.Point(222, 313);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(185, 34);
            this.btnCalcular.TabIndex = 4;
            this.btnCalcular.Text = "CALCULAR MULTA";
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcularMulta);
            // 
            // lblresultado
            // 
            this.lblresultado.AutoSize = true;
            this.lblresultado.BackColor = System.Drawing.Color.Maroon;
            this.lblresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblresultado.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblresultado.Location = new System.Drawing.Point(327, 147);
            this.lblresultado.Name = "lblresultado";
            this.lblresultado.Size = new System.Drawing.Size(18, 20);
            this.lblresultado.TabIndex = 5;
            this.lblresultado.Text = "$";
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(530, 360);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 6;
            this.btnCerrar.Text = "CERRAR";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // lblSigno
            // 
            this.lblSigno.AutoSize = true;
            this.lblSigno.BackColor = System.Drawing.Color.Maroon;
            this.lblSigno.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSigno.ForeColor = System.Drawing.SystemColors.Control;
            this.lblSigno.Location = new System.Drawing.Point(303, 147);
            this.lblSigno.Name = "lblSigno";
            this.lblSigno.Size = new System.Drawing.Size(18, 20);
            this.lblSigno.TabIndex = 7;
            this.lblSigno.Text = "$";
            // 
            // lblpesos
            // 
            this.lblpesos.AutoSize = true;
            this.lblpesos.BackColor = System.Drawing.Color.Maroon;
            this.lblpesos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpesos.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblpesos.Location = new System.Drawing.Point(370, 149);
            this.lblpesos.Name = "lblpesos";
            this.lblpesos.Size = new System.Drawing.Size(53, 20);
            this.lblpesos.TabIndex = 8;
            this.lblpesos.Text = "Pesos";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(627, 395);
            this.Controls.Add(this.lblpesos);
            this.Controls.Add(this.lblSigno);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.lblresultado);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblPagarMulta);
            this.Controls.Add(this.lblSelecMulta);
            this.Controls.Add(this.cbxSelecMults);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxSelecMults;
        private System.Windows.Forms.Label lblSelecMulta;
        private System.Windows.Forms.Label lblPagarMulta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label lblresultado;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Label lblSigno;
        private System.Windows.Forms.Label lblpesos;
    }
}

