﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1Prog01
{
    public partial class FormIntendente : Form
    {
        public FormIntendente()
        {
            InitializeComponent();
        }

        public void btnCalcular_Click(object sender, EventArgs e)
        {
            Intendentes ObjI = new Intendentes();
            int HorasTrabajadasI = int.Parse(txtHtrabajadas.Text);
            int HorasExtrasI = int.Parse(txtHextras.Text);
            
            ObjI.SalarioSemanal(HorasExtrasI, HorasTrabajadasI);
            lblSsemanal.Text = "El salario semanal es: " + ObjI.X;



        }
    }
}
