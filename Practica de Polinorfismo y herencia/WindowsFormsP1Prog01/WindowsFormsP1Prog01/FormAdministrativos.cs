﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1Prog01
{
    public partial class FormAdministrativos : Form
    {
        public FormAdministrativos()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Administrativos Obja = new Administrativos();
            int HorasTrabajadasI = int.Parse(txtHtrabajadas.Text);
            int HorasExtrasI = int.Parse(txtHextras.Text);

            Obja.SalarioSemanal(HorasExtrasI, HorasTrabajadasI);
            lblSsemanal.Text = "El salario semanal es: " + Obja.X;
        }
    }
}
