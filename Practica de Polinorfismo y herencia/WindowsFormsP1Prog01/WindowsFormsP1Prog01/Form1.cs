﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1Prog01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cboLista.SelectedIndex = 0;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCalcular_Click_1(object sender, EventArgs e)
        {

            Form fmr = null;
            switch (cboLista.SelectedIndex)
            {
                case 0:
                    fmr = new FormIntendente();
                    break;
                case 1:
                    fmr = new FormProduccion();
                    break;
                case 2:
                    fmr = new FormAdministrativos();
                    break;
                case 3:
                    fmr = new FormGerencias();
                    break;
                case 4:
                    fmr = new FormDueños();
                    break;
            }
            Hide();
            fmr?.ShowDialog();
            fmr.Dispose();
            Show();

        }
    }
}
