﻿
namespace WindowsFormsP1Prog01
{
    partial class FormIntendente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalcular = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHtrabajadas = new System.Windows.Forms.TextBox();
            this.txtHextras = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSsemanal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(209, 245);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 0;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(172, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Intendente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ingresa Horas Trabajadas";
            // 
            // txtHtrabajadas
            // 
            this.txtHtrabajadas.Location = new System.Drawing.Point(209, 70);
            this.txtHtrabajadas.Name = "txtHtrabajadas";
            this.txtHtrabajadas.Size = new System.Drawing.Size(100, 20);
            this.txtHtrabajadas.TabIndex = 3;
            // 
            // txtHextras
            // 
            this.txtHextras.Location = new System.Drawing.Point(209, 140);
            this.txtHextras.Name = "txtHextras";
            this.txtHextras.Size = new System.Drawing.Size(100, 20);
            this.txtHextras.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Ingrese Horas Extras";
            // 
            // lblSsemanal
            // 
            this.lblSsemanal.AutoSize = true;
            this.lblSsemanal.Location = new System.Drawing.Point(54, 209);
            this.lblSsemanal.Name = "lblSsemanal";
            this.lblSsemanal.Size = new System.Drawing.Size(83, 13);
            this.lblSsemanal.TabIndex = 6;
            this.lblSsemanal.Text = "Salario Semanal";
            // 
            // FormIntendente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 298);
            this.Controls.Add(this.lblSsemanal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtHextras);
            this.Controls.Add(this.txtHtrabajadas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCalcular);
            this.Name = "FormIntendente";
            this.Text = "Intendente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtHtrabajadas;
        private System.Windows.Forms.TextBox txtHextras;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSsemanal;
    }
}