﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prog1
{
    public partial class Empleado_por_Comision : Form
    {
        public Empleado_por_Comision()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            float a = float.Parse(txtVentas.Text);
            float b = float.Parse(txtComision.Text);
            string nom = txtNombre.Text;
            string ap = TxtApellido.Text;
            string nss = txtNSS.Text;
            EmpleadoHora objE = new EmpleadoHora();
            objE.CalculaSalario(a, b, 0, 0);
            lsDatos.Items.Add("Nombre del Empleado: " + nom);
            lsDatos.Items.Add("Apellido del Empleado: " + ap);
            lsDatos.Items.Add("Numero Seguro Social: " + nss);
            lsDatos.Items.Add("Numero de Ventas: " + a);
            lsDatos.Items.Add("Total a Pagar: " + objE.X);
        }

       
    }
}
