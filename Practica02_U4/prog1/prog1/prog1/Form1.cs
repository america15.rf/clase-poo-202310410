﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prog1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cboLista.SelectedIndex = 0;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Form fmr = null;
            switch (cboLista.SelectedIndex)
            {
                case 0:
                    fmr = new Empleado_Asalareado();
                    break;
                case 1:
                    fmr = new Empleado_por_Hora();
                    break;
                case 2:
                    fmr = new Empleado_por_Comision();
                    break;
                case 3:
                    fmr = new Empleado_por_Comision_y_con_Salario();
                    break;

            }
            Hide();
            fmr?.ShowDialog();
            fmr.Dispose();
        }
    }
}
