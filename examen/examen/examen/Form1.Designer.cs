﻿namespace examen
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnIniciar = new System.Windows.Forms.Button();
            this.btnCerr = new System.Windows.Forms.Button();
            this.btnLimp = new System.Windows.Forms.Button();
            this.lstPropiedades = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnIniciar
            // 
            this.btnIniciar.Location = new System.Drawing.Point(54, 272);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(98, 23);
            this.btnIniciar.TabIndex = 0;
            this.btnIniciar.Text = "INICIAR CLASE";
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // btnCerr
            // 
            this.btnCerr.Location = new System.Drawing.Point(225, 272);
            this.btnCerr.Name = "btnCerr";
            this.btnCerr.Size = new System.Drawing.Size(75, 23);
            this.btnCerr.TabIndex = 1;
            this.btnCerr.Text = "CERRAR";
            this.btnCerr.UseVisualStyleBackColor = true;
            this.btnCerr.Click += new System.EventHandler(this.btnCerr_Click);
            // 
            // btnLimp
            // 
            this.btnLimp.Location = new System.Drawing.Point(362, 272);
            this.btnLimp.Name = "btnLimp";
            this.btnLimp.Size = new System.Drawing.Size(75, 23);
            this.btnLimp.TabIndex = 2;
            this.btnLimp.Text = "LIMPIAR";
            this.btnLimp.UseVisualStyleBackColor = true;
            this.btnLimp.Click += new System.EventHandler(this.btnLimp_Click);
            // 
            // lstPropiedades
            // 
            this.lstPropiedades.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstPropiedades.FormattingEnabled = true;
            this.lstPropiedades.Location = new System.Drawing.Point(130, 61);
            this.lstPropiedades.Name = "lstPropiedades";
            this.lstPropiedades.Size = new System.Drawing.Size(254, 130);
            this.lstPropiedades.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(507, 307);
            this.Controls.Add(this.lstPropiedades);
            this.Controls.Add(this.btnLimp);
            this.Controls.Add(this.btnCerr);
            this.Controls.Add(this.btnIniciar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Button btnCerr;
        private System.Windows.Forms.Button btnLimp;
        private System.Windows.Forms.ListBox lstPropiedades;
    }
}

