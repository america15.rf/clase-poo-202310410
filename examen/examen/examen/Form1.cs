﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace examen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            ClassLAP LAP = new ClassLAP();

            LAP.setTamanio(14);
            int tamanioTV1 = LAP.getTamanio();

            LAP.settecladoNumerico(1);
            int TecladoNumericoLAP = LAP.gettecladoNumerico();

            LAP.setpcamara(16);
            int PCamaraLAP = LAP.getpcamara();

            LAP.setram(4);
            int RAMLAP = LAP.getram();

            LAP.setcolor("Gris");
            string ColorLAP = LAP.getcolor();

            lstPropiedades.Items.Add("Tamaño LAP: " + tamanioTV1);
            lstPropiedades.Items.Add("Teclado Numerico LAP: " + TecladoNumericoLAP);
            lstPropiedades.Items.Add("Pixeles de Camara: " + PCamaraLAP);
            lstPropiedades.Items.Add("Memoria RAM: " + RAMLAP);
            lstPropiedades.Items.Add("Color LAP: " + ColorLAP);
        }

        private void btnCerr_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLimp_Click(object sender, EventArgs e)
        {
            lstPropiedades.Items.Clear();
        }
    }
}
