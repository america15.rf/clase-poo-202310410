﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace examen
{
    class ClassLAP
    {
        public int tamanio = 0;
        public int TecladoNumerico =0;
        public int PCamara = 0;
        public int RAM = 0;
        public string Color = "";

        public void setTamanio(int tamanio)
        {
            this.tamanio = tamanio;
        }

        public int getTamanio()
        {
            return this.tamanio;
        }

        public void settecladoNumerico(int TecladoNumerico)
        {
            this.TecladoNumerico = TecladoNumerico;
        }

        public int gettecladoNumerico()
        {
            return this.TecladoNumerico;
        }

        public void setpcamara(int PCamara)
        {
            this.PCamara = PCamara;
        }

        public int getpcamara()
        {
            return this.PCamara;
        }

        public void setram(int RAM)
        {
            this.RAM = RAM;
        }

        public int getram()
        {
            return this.RAM;
        }

        public void setcolor(string Color)
        {
            this.Color = Color;
        }

        public string getcolor()
        {
            return this.Color;
        }
    }
}
