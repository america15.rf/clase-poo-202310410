﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog05
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public string Lunes;
        public int Hrs;
        public int min;
        public string descrip;
        private void btnAgendar_Click(object sender, EventArgs e)
        {
            

            CITA o = new CITA();
            
            try
            {


                Lunes = Convert.ToString(txtDiaS.Text);
                Hrs = Convert.ToInt32(txtHora.Text);
                min = Convert.ToInt32(txtMin.Text);
                descrip = Convert.ToString(txtDes.Text);
            }
            catch 
            {
                MessageBox.Show("Error de Argumento", "ArgumentException", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtDiaS.Clear();
            txtHora.Clear();
            txtMin.Clear();
            txtDes.Clear();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            lstAgendado.Items.Clear();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
