﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog4
{
    class Program
    {
        static void Main(string[] args)
        {
            int A;
            int B;
            int Res = 0;
            Console.WriteLine("Suma de 2 Datos");
            Console.WriteLine();
            Console.WriteLine("Introducir A y B");
            A = int.Parse(Console.ReadLine());
            B = int.Parse(Console.ReadLine());
            
            
            try
            {
                Res = A+B;
            }
            catch (Exception )
            {
                
                throw new Exception("Hay un Problema, No insertaste el Formato Correcto");
            }
            finally
            {
                Console.WriteLine(A + "+" + B + "=" + Res);
            }
            Console.ReadKey();
        }
    }
}
