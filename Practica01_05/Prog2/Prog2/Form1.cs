﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                double Numero1 = 0;
                double Numero2 = 0;
                double Resultado = 0;

                Numero1 = Convert.ToDouble(txtNun1.Text);
                Numero2 = Convert.ToDouble(txtNum2.Text);
                Resultado = Numero1 - Numero2;
                txtRes.Text = Convert.ToString(Resultado);

            }
            catch
            {
                MessageBox.Show("Favor de llenar todos los Campos...", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                MessageBox.Show("La Tarea fue completada", "Felicitaciones", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtNun1.Text = ("");
            txtNum2.Text = ("");
            txtRes.Text = ("");
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
