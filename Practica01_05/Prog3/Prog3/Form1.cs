﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string N;
                string AP;
                string AM;
                string EstadoC;
                int Tel = 0;

                N = Convert.ToString(txtName.Text);
                AP = Convert.ToString(txtApeP.Text);
                AM = Convert.ToString(txtEC.Text);
                EstadoC = Convert.ToString(txtEC.Text);
                Tel = Convert.ToInt32(txtTelefono.Text);

                
            }
            catch
            {
                if (txtName.Text == "")
                {
                    if (txtApeP.Text == "")
                    {
                        if (txtApeM.Text == "")
                        {
                            if (txtEC.Text == "")
                            {
                                if (txtTelefono.Text == "")
                                {
                                    MessageBox.Show("No puedes dejar NADA en Blanco", "Indice fuera de Rango", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }

                            }

                        }

                    }

                }
                else
                {
                    MessageBox.Show("Ingrese los Datos Correctos", "Excepcion de Formato", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                

            }
            lstGuardado.Items.Add(txtName.Text);
            lstGuardado.Items.Add(txtApeP.Text);
            lstGuardado.Items.Add(txtApeM.Text);
            lstGuardado.Items.Add(txtEC.Text);
            lstGuardado.Items.Add(txtTelefono.Text);




        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtName.Text = ("");
            txtApeP.Text = ("");
            txtApeM.Text = ("");
            txtEC.Text = ("");
            txtTelefono.Text = ("");
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            lstGuardado.Items.Clear();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
