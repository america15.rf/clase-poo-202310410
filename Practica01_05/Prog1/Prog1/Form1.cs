﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                double numero1 = 0;
                double numero2 = 0;
                double resultado = 0;

                numero1 = Convert.ToDouble(txtNum1.Text);
                numero2 = Convert.ToDouble(txtNum2.Text);
                resultado = numero1 + numero2;
                txtRes.Text = Convert.ToString(resultado);
            }
            catch
            {
                MessageBox.Show("Favor de Llenar Todos los Campos...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("Favor de Insertar Numeros NO Letras...", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtNum1.Text=("");
            txtNum2.Text = ("");
            txtRes.Text = ("");
        }
    }
}
