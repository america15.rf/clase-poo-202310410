﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace practica02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            Class_TV TV1 = new Class_TV();
            TV1.setTamanio(30);
            int tamanioTV1 = TV1.getTamanio();

            TV1.setVolumen(10);
            int volumenTV1 = TV1.getVolumen();

            TV1.setColor("Blue");
            string colorTV1 = TV1.getColor();

            TV1.setContraste(2);
            int contrasteTV1 = TV1.getContraste();

            TV1.setMarca("LG");
            string marcaTV1 = TV1.getMarca();

           

            lstDates.Items.Add("Tamaño TV1: " + tamanioTV1);
            lstDates.Items.Add("Volumen TV1: " + volumenTV1);
            lstDates.Items.Add("Color TV1: " + colorTV1);
            lstDates.Items.Add("Contraste TV1: " + contrasteTV1);
            lstDates.Items.Add("Marca TV1: " + marcaTV1);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            lstDates.Items.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
