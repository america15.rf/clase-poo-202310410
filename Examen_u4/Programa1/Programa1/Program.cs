﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa1
{
    class Program
    {
        static void Main(string[] args)
        {
            Banco banco = new Banco(1000000);

            Console.WriteLine(banco.Operar());
            Console.WriteLine(banco.Operar(120000));
            Console.WriteLine(banco.Operar());
            Console.WriteLine(banco.Operar(500000, "ABC123"));
            Console.WriteLine(banco.Operar());

            Console.ReadKey();
        }
    }
}
