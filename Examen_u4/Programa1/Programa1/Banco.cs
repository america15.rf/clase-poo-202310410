﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programa1
{
    class Banco
    {
        public Banco(int saldo)
        {
            Saldo = saldo;
        }
        public int Saldo { get; set; }
        public string Operar()
        {
            return "Su saldo actual es: " + Saldo;
        }
        public string Operar(int valorRetiro)
        {
            Saldo = Saldo + valorRetiro;
            return "Se ha retirado de su cuenta el valor de: " + valorRetiro;
        }
        public string Operar(int valorTransferencia, string numeroCuenta)
        {
            Saldo = Saldo - valorTransferencia;
            return "Se ha transferido a la cuenta: " + numeroCuenta + "El valor de: " + valorTransferencia;
        }
    }
}
